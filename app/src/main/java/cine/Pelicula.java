package cine;

import java.util.List;

public class Pelicula {
    private String genero;
    private String titulo;
    private Integer duracion;
    private Integer edad_min;
    private List<ActorPrincipal> actores_principales;
    private List<Director> directores;
    public Pelicula(String genero, String titulo, Integer duracion, Integer edad_min,
            List<ActorPrincipal> actores_principales, List<Director> directores) {
        this.genero = genero;
        this.titulo = titulo;
        this.duracion = duracion;
        this.edad_min = edad_min;
        this.actores_principales = actores_principales;
        this.directores = directores;
    }
    public String getGenero() {
        return genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public Integer getDuracion() {
        return duracion;
    }
    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }
    public Integer getEdad_min() {
        return edad_min;
    }
    public void setEdad_min(Integer edad_min) {
        this.edad_min = edad_min;
    }
    public List<ActorPrincipal> getActores_principales() {
        return actores_principales;
    }
    public void setActores_principales(List<ActorPrincipal> actores_principales) {
        this.actores_principales = actores_principales;
    }
    public List<Director> getDirectores() {
        return directores;
    }
    public void setDirectores(List<Director> directores) {
        this.directores = directores;
    }
    
}
