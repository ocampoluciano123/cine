package cine;

public class Persona {
    private String nombre_completo;
    private Integer edad;
    public Persona(String nombre_completo, Integer edad) {
        this.nombre_completo = nombre_completo;
        this.edad = edad;
    }
    public String getNombre_completo() {
        return nombre_completo;
    }
    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }
    public Integer getEdad() {
        return edad;
    }
    public void setEdad(Integer edad) {
        this.edad = edad;
    }
}
