package cine;

import java.time.LocalDate;
import java.util.List;

public class Programacion {
    private LocalDate fecha_inicio;
    private LocalDate fecha_fin;
    private List<Funcion> funciones;
    public Programacion(LocalDate fecha_inicio, LocalDate fecha_fin, List<Funcion> funciones) {
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.funciones = funciones;
    }
    public LocalDate getFecha_inicio() {
        return fecha_inicio;
    }
    public void setFecha_inicio(LocalDate fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    public LocalDate getFecha_fin() {
        return fecha_fin;
    }
    public void setFecha_fin(LocalDate fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    public List<Funcion> getFunciones() {
        return funciones;
    }
    public void setFunciones(List<Funcion> funciones) {
        this.funciones = funciones;
    }

    
}
