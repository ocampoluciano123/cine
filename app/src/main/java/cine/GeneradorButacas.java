package cine;

import java.util.ArrayList;
import java.util.List;

public class GeneradorButacas {
    public static void getResetearButacas(Funcion funcion) {
        List<Butaca> butacas = new ArrayList<Butaca>();
        List<String> letras = new ArrayList<String>();
        letras.add("A");
        letras.add("B");
        letras.add("C");
        letras.add("D");
        letras.add("E");
        letras.add("F");
        letras.add("G");
        letras.add("H");
        int cont = 1;
        for (int x = 0; letras.size() > x; x++) {
            for (int i = 0; 10 > i; i++) {
                Butaca butaca = new Butaca(letras.get(x) + cont, true);
                butacas.add(butaca);
                cont++;
            }
        }
        for (int y = 0; butacas.size() > y; y++) {
            System.out.println(butacas.get(y).getNro_butaca());
            System.out.println(butacas.get(y).getDisponible());
        }
        funcion.setButacas(butacas);
    }
    public static List<Butaca> getGeneradorButacas() {
        List<Butaca> butacas = new ArrayList<Butaca>();
        List<String> letras = new ArrayList<String>();
        letras.add("A");
        letras.add("B");
        letras.add("C");
        letras.add("D");
        letras.add("E");
        letras.add("F");
        letras.add("G");
        letras.add("H");
        int cont = 1;
        for (int x = 0; letras.size() > x; x++) {
            for (int i = 0; 10 > i; i++) {
                Butaca butaca = new Butaca(letras.get(x) + cont, true);
                butacas.add(butaca);
                cont++;
            }
        }
        /*for (int y = 0; butacas.size() > y; y++) {
            System.out.println(butacas.get(y).getNro_butaca());
            System.out.println(butacas.get(y).getDisponible());
        }*/
        return butacas;
    }
}
