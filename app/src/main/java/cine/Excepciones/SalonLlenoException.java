package cine.Excepciones;

public class SalonLlenoException extends RuntimeException{
    public SalonLlenoException(String mensaje) {
        super(mensaje);
    }
}
