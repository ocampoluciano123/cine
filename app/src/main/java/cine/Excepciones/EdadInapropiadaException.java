package cine.Excepciones;

public class EdadInapropiadaException extends RuntimeException{
    public EdadInapropiadaException(String mensaje) {
        super(mensaje);
    }
}
