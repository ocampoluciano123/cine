package cine.Excepciones;

public class PeliculaInexistenteException extends RuntimeException{
    public PeliculaInexistenteException(String mensaje) {
        super(mensaje);
    }
}
