package cine;

import java.util.Date;
import java.time.LocalDate;

public class Entrada {
    private Integer nro_ticket;
    private Integer nro_funcion;
    private String nro_butaca;
    private final Date fecha_venta = new Date();
    private String nombre_pelicula;
    private LocalDate fecha_funcion;
    private Integer hora_funcion;
    private Double costo;
    private Integer edad_min;
    private String calificacion;
    
    public Entrada() {
    }

    public Entrada(Integer nro_ticket, Integer nro_funcion, String nro_butaca,String nombre_pelicula, LocalDate fecha_funcion,
            Integer hora_funcion, Double costo, Integer edad_min, String calificacion) {
        this.nro_ticket = nro_ticket;
        this.nro_funcion = nro_funcion;
        this.nro_butaca = nro_butaca;
        this.nombre_pelicula = nombre_pelicula;
        this.fecha_funcion = fecha_funcion;
        this.hora_funcion = hora_funcion;
        this.costo = costo;
        this.edad_min = edad_min;
        this.calificacion = calificacion;
    }
    public Integer getNro_ticket() {
        return nro_ticket;
    }
    public void setNro_ticket(Integer nro_ticket) {
        this.nro_ticket = nro_ticket;
    }
    public Integer getNro_funcion() {
        return nro_funcion;
    }
    public void setNro_funcion(Integer nro_funcion) {
        this.nro_funcion = nro_funcion;
    }
    public String getNro_butaca() {
        return nro_butaca;
    }

    public void setNro_butaca(String nro_butaca) {
        this.nro_butaca = nro_butaca;
    }
    public String getNombre_pelicula() {
        return nombre_pelicula;
    }
    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }
    public LocalDate getFecha_funcion() {
        return fecha_funcion;
    }
    public void setFecha_funcion(LocalDate fecha_funcion) {
        this.fecha_funcion = fecha_funcion;
    }
    public Integer getHora_funcion() {
        return hora_funcion;
    }
    public void setHora_funcion(Integer hora_funcion) {
        this.hora_funcion = hora_funcion;
    }
    public Double getCosto() {
        return costo;
    }
    public void setCosto(Double costo) {
        this.costo = costo;
    }
    public Integer getEdad_min() {
        return edad_min;
    }
    public void setEdad_min(Integer edad_min) {
        this.edad_min = edad_min;
    }
    public String getCalificacion() {
        return calificacion;
    }
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }
    public Date getFecha_venta() {
        return fecha_venta;
    }

    
}
