package cine;

public class Butaca {
    private String nro_butaca;
    private Boolean disponible;
    public Butaca(String nro_butaca, Boolean disponible) {
        this.nro_butaca = nro_butaca;
        this.disponible = disponible;
    }
    public String getNro_butaca() {
        return nro_butaca;
    }
    public void setNro_butaca(String nro_butaca) {
        this.nro_butaca = nro_butaca;
    }
    public Boolean getDisponible() {
        return disponible;
    }
    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }
    
}