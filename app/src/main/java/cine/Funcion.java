package cine;

import java.time.LocalDate;
import java.util.List;

public class Funcion {
    private Integer nro_funcion;
    private LocalDate fecha_funcion;
    private Integer hora_inicio_funcion;
    private Integer hora_fin_funcion;
    private Pelicula pelicula_funcion;
    private Integer entradas_registradas;
    private List<Butaca> butacas;
    private String calificacion_pelicula;
    public Funcion(Integer nro_funcion, LocalDate fecha_funcion, Integer hora_inicio_funcion, Integer hora_fin_funcion,
            Pelicula pelicula_funcion, Integer entradas_registradas, List<Butaca> butacas,
            String calificacion_pelicula) {
        this.nro_funcion = nro_funcion;
        this.fecha_funcion = fecha_funcion;
        this.hora_inicio_funcion = hora_inicio_funcion;
        this.hora_fin_funcion = hora_fin_funcion;
        this.pelicula_funcion = pelicula_funcion;
        this.entradas_registradas = entradas_registradas;
        this.butacas = butacas;
        this.calificacion_pelicula = calificacion_pelicula;
    }
    public Integer getNro_funcion() {
        return nro_funcion;
    }
    public void setNro_funcion(Integer nro_funcion) {
        this.nro_funcion = nro_funcion;
    }
    public LocalDate getFecha_funcion() {
        return fecha_funcion;
    }
    public void setFecha_funcion(LocalDate fecha_funcion) {
        this.fecha_funcion = fecha_funcion;
    }
    public Integer getHora_inicio_funcion() {
        return hora_inicio_funcion;
    }
    public void setHora_inicio_funcion(Integer hora_inicio_funcion) {
        this.hora_inicio_funcion = hora_inicio_funcion;
    }
    public Integer getHora_fin_funcion() {
        return hora_fin_funcion;
    }
    public void setHora_fin_funcion(Integer hora_fin_funcion) {
        this.hora_fin_funcion = hora_fin_funcion;
    }
    public Pelicula getPelicula_funcion() {
        return pelicula_funcion;
    }
    public void setPelicula_funcion(Pelicula pelicula_funcion) {
        this.pelicula_funcion = pelicula_funcion;
    }
    public Integer getEntradas_registradas() {
        return entradas_registradas;
    }
    public void setEntradas_registradas(Integer entradas_registradas) {
        this.entradas_registradas = entradas_registradas;
    }
    public List<Butaca> getButacas() {
        return butacas;
    }
    public void setButacas(List<Butaca> butacas) {
        this.butacas = butacas;
    }
    public String getCalificacion_pelicula() {
        return calificacion_pelicula;
    }
    public void setCalificacion_pelicula(String calificacion_pelicula) {
        this.calificacion_pelicula = calificacion_pelicula;
    }

    
}
