package cine;

public class Espectador extends Persona{
    private Double dinero;
    private Entrada entrada;

    public Espectador(String nombre_completo, Integer edad, Double dinero) {
        super(nombre_completo, edad);
        this.dinero = dinero;
    }

    public Double getDinero() {
        return dinero;
    }

    public void setDinero(Double dinero) {
        this.dinero = dinero;
    }

    public Entrada getEntrada() {
        return entrada;
    }

    public void setSolicitarEntrada(Cine cine, String nombre_pelicula) {
        this.entrada = cine.getSolicitudEntrada(getEdad(), nombre_pelicula, dinero);
    }
}
