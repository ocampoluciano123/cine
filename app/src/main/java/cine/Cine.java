package cine;

import java.util.List;
import java.util.Random;

import cine.Excepciones.EdadInapropiadaException;
import cine.Excepciones.PeliculaInexistenteException;
import cine.Excepciones.SalonLlenoException;

public class Cine {
    private String nombre_cine;
    private final Integer capacidad_cine;
    private List<Programacion> programaciones;
    private Double precio_entrada;

    public Cine(String nombre_cine, Integer capacidad_cine, List<Programacion> programaciones, Double precio_entrada) {
        this.nombre_cine = nombre_cine;
        this.capacidad_cine = capacidad_cine;
        this.programaciones = programaciones;
        this.precio_entrada = precio_entrada;
    }

    public String getNombre_cine() {
        return nombre_cine;
    }

    public void setNombre_cine(String nombre_cine) {
        this.nombre_cine = nombre_cine;
    }

    public List<Programacion> getProgramaciones() {
        return programaciones;
    }

    public void setProgramaciones(List<Programacion> programaciones) {
        this.programaciones = programaciones;
    }

    public Double getPrecio_entrada() {
        return precio_entrada;
    }

    public void setPrecio_entrada(Double precio_entrada) {
        this.precio_entrada = precio_entrada;
    }

    public Integer getCapacidad_cine() {
        return capacidad_cine;
    }

    public Entrada getGenerarEntrada(String nombre_pelicula, Double dinero_disponible, int aux1,
            int aux2, String butaca) {
        Random numAleatorio = new Random();
        int nro_random = numAleatorio.nextInt(10000) + 1;
        Entrada entrada = new Entrada(nro_random,
                programaciones.get(aux1).getFunciones().get(aux2).getNro_funcion(), butaca, nombre_pelicula,
                programaciones.get(aux1).getFunciones().get(aux2).getFecha_funcion(),
                programaciones.get(aux1).getFunciones().get(aux2).getHora_inicio_funcion(), getPrecio_entrada(),
                programaciones.get(aux1).getFunciones().get(aux2).getPelicula_funcion().getEdad_min(),
                programaciones.get(aux1).getFunciones().get(aux2).getCalificacion_pelicula());
        return entrada;
    }

    public String getAsignarButaca(String butaca, int aux1, int aux2) {
        do {
            Random numAleatorio = new Random();
            int nro_random = numAleatorio.nextInt(programaciones.get(aux1).getFunciones().get(aux2).getButacas().size());
            if (programaciones.get(aux1).getFunciones().get(aux2).getButacas().get(nro_random)
                    .getDisponible() == true) {
                butaca = programaciones.get(aux1).getFunciones().get(aux2).getButacas().get(nro_random).getNro_butaca();
                programaciones.get(aux1).getFunciones().get(aux2).getButacas().get(nro_random).setDisponible(false);
            }
        } while (butaca == null);
        return butaca;
    }

    public Integer getAsientosDisponibles(int aux1, int aux2, Integer asiento_disponible) throws SalonLlenoException {
        if (programaciones.get(aux1).getFunciones().get(aux2).getEntradas_registradas() < programaciones.get(aux1).getFunciones().get(aux2).getButacas().size()) {
            asiento_disponible = 1;
            programaciones.get(aux1).getFunciones().get(aux2).setEntradas_registradas(1);
        } else {
            throw new SalonLlenoException("Funcion completa");
        }
        return asiento_disponible;
    }

    public Integer getValidarEdad(Integer edad, int aux1, int aux2, Integer edad_minima) throws EdadInapropiadaException{
        if (edad > programaciones.get(aux1).getFunciones().get(aux2).getPelicula_funcion().getEdad_min()) {
            edad_minima = 1;
        } else {
            throw new EdadInapropiadaException("Pelicula no apta para el solicitante");
        }
        return edad_minima;
    }

    public Entrada getSolicitudEntrada(Integer edad, String nombre_pelicula, Double dinero_disponible) throws PeliculaInexistenteException {
        Entrada entrada = new Entrada();
        int aux1 = 0, aux2 = 0;
        String butaca = null;
        Integer edad_minima = null, existe_pelicula = null, asiento_disponible = null;

        for (int i = 0; programaciones.size() > i; i++) {
            for (int x = 0; programaciones.get(i).getFunciones().size() > x; x++) {
                if (programaciones.get(i).getFunciones().get(x).getPelicula_funcion().getTitulo()
                        .toUpperCase().equals(nombre_pelicula.toUpperCase())) {
                    existe_pelicula = 1;
                    aux1 = i;
                    aux2 = x;
                }
            }
        }

        if (existe_pelicula !=null) {
            edad_minima= getValidarEdad(edad, aux1, aux2, edad_minima);
        } else {
            throw new PeliculaInexistenteException("Pelicula Inexistente");
        }
        
        asiento_disponible = getAsientosDisponibles(aux1, aux2, asiento_disponible);
        butaca = getAsignarButaca(butaca, aux1, aux2);
        entrada = getGenerarEntrada(nombre_pelicula, dinero_disponible, aux1, aux2, butaca);

        return entrada;
    }
}

