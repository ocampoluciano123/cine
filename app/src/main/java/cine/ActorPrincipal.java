package cine;

public class ActorPrincipal extends Persona{
    private String nombre_personaje;

    public ActorPrincipal(String nombre_completo, Integer edad, String nombre_personaje) {
        super(nombre_completo, edad);
        this.nombre_personaje = nombre_personaje;
    }

    public String getNombre_personaje() {
        return nombre_personaje;
    }

    public void setNombre_personaje(String nombre_personaje) {
        this.nombre_personaje = nombre_personaje;
    }

}
