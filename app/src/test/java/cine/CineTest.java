package cine;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CineTest {
    @Test
    public void testeoCine() {
        Director director = new Director("Jose Madero", 45);
        List<Director> directores = new ArrayList<Director>();
        directores.add(director);

        List<ActorPrincipal> actores = new ArrayList<ActorPrincipal>();
        ActorPrincipal actor1 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        ActorPrincipal actor2 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        actores.add(actor1);
        actores.add(actor2);

        List<Butaca> butacas = new ArrayList<Butaca>();
        butacas = GeneradorButacas.getGeneradorButacas();


        Pelicula pelicula1 = new Pelicula("Terror", "Actividad Paranormal", 2, 18, actores, directores);


        List<Funcion> funciones = new ArrayList<Funcion>();
        LocalDate fecha_funcion= LocalDate.of(2023, 06, 01);
        Funcion funcion1 = new Funcion(1234, fecha_funcion, 18, 20, pelicula1, 0, butacas, "5 estrellas");
        funciones.add(funcion1);

        List<Programacion> programaciones = new ArrayList<Programacion>();
        Programacion programacion1= new Programacion(fecha_funcion, fecha_funcion, funciones);
        programaciones.add(programacion1);
        Cine cine = new Cine("CinemaCenter", 80, programaciones, 500.00);
        assertEquals(cine.getCapacidad_cine().toString(), "80");
    }
}
