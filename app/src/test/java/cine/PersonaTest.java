package cine;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PersonaTest {
    @Test
    public void verificarPersona() {
        Persona persona = new Persona("Martin Ocampo", 18);
        assertEquals(persona.getEdad().toString(), "18");;
    }
}
