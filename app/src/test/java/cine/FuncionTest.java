package cine;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class FuncionTest {
    @Test
    public void verificarFuncion() {
        Director director = new Director("Jose Madero", 45);
        List<Director> directores = new ArrayList<Director>();
        directores.add(director);

        List<ActorPrincipal> actores = new ArrayList<ActorPrincipal>();
        ActorPrincipal actor1 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        ActorPrincipal actor2 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        actores.add(actor1);
        actores.add(actor2);

        List<Butaca> butacas = new ArrayList<Butaca>();
        butacas = GeneradorButacas.getGeneradorButacas();


        Pelicula pelicula1 = new Pelicula("Terror", "Actividad Paranormal", 2, 18, actores, directores);


        LocalDate fecha_funcion= LocalDate.of(2023, 06, 01);
        Funcion funcion1 = new Funcion(1234, fecha_funcion, 18, 20, pelicula1, 0, butacas, "5 Estrellas");

        assertEquals(funcion1.getCalificacion_pelicula(), "5 Estrellas");
    }

}
