package cine;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ActorPrincipalTest {
    @Test
    public void verificarActor() {
        ActorPrincipal actor = new ActorPrincipal("Juan Andrada", 18, "Belicho");
        assertEquals(actor.getNombre_personaje(), "Belicho");
    }
}
