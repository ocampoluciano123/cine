package cine;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class EntradaTest {
    @Test
    public void testeoEntrada() {
        LocalDate fech_funcion = LocalDate.of(2023,6, 28);
        Entrada entrada = new Entrada(123, 123, "A5", "Actividad Paranormal", fech_funcion, 18, 500.00, 18,
                "5 Estrellas");
        
        assertEquals(entrada.getCalificacion(), "5 Estrellas");
    }

}
