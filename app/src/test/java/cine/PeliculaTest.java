package cine;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class PeliculaTest {
    @Test
    public void testeoPeli() {
        Director director = new Director("Jose Madero", 45);
        List<Director> directores = new ArrayList<Director>();
        directores.add(director);

        List<ActorPrincipal> actores = new ArrayList<ActorPrincipal>();
        ActorPrincipal actor1 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        ActorPrincipal actor2 = new ActorPrincipal("Juan Pedraza", 18, "Manuel");
        actores.add(actor1);
        actores.add(actor2);
        
        Pelicula pelicula1 = new Pelicula("Terror", "Actividad Paranormal", 2, 18, actores, directores);
        assertEquals(pelicula1.getGenero(), "Terror");
    }
}
