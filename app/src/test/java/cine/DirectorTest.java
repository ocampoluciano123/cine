package cine;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DirectorTest {
    @Test
    public void verificarDirector() {
        Director director = new Director("Jose Madero", 34);
        assertEquals(director.getNombre_completo(), "Jose Madero");
    }
}
